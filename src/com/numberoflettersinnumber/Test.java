package com.numberoflettersinnumber;

import java.util.ArrayList;
import java.util.List;

public class Test {

	public static void main(String[] args) {
	}

	static String convertDecimalToBinary(long start) {
		String res = "";
		while (start != 0) {
			res += start % 2;
			start = start / 2;
		}
		return res;
	}

	static long test(long start, long n) {
		int i = 0;

		List<Long> starts = new ArrayList<>();
		while (i < n) {
			String convFromdecimalToBinary = convertDecimalToBinary(start);
			String convFromBinaryToreprOfWords = convertFromBinaryToRepresentationOfWords(convFromdecimalToBinary);
			start = convFromBinaryToreprOfWords.length();
			starts.add(start);
			if (starts.size() > 1) {
				if (starts.get(starts.size() - 2) == starts.get(starts.size() - 1)) {
					return start;
				}
			}
			i++;
		}
		return start;
	}

	// convert from binary to a representation of words(zero,one)
	static String convertFromBinaryToRepresentationOfWords(String startBinary) {
		String res = "";
		for (int i = 0; i < startBinary.length(); i++) {
			if (startBinary.charAt(i) == '1') {
				res += "one";
			} else {
				res += "zero";
			}
		}
		return res;
	}
}
